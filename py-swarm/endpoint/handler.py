from endpoint import swarmIntelligence_pb2_grpc
from endpoint import swarmIntelligence_pb2
from google import protobuf
from tensorforce import Agent
import numpy as np


class SwarmIntelligenceServicer(swarmIntelligence_pb2_grpc.SwarmIntelligenceServicer):

    def write_traces(self):
        q = {}
        for i in range(len(self.agents)):
            print(f'Writing trace for agent {i}')
            np.save(f'data/agent-{i}-trace', np.array(self.traces[i], dtype=object), allow_pickle=True)

            print(f'Writing tensors for agent {i}')
            for state_hash, state in self.observed_states.items():
                self.agents[i].act(states=state, independent=True, deterministic=True)
                action_values = self.agents[i].tracked_tensors()['agent/policy/action-values']
                q[state_hash] = action_values
            np.save(f'data/agent-{i}-q', q, allow_pickle=True)

    def __init__(self):
        self.traces          = {}
        self.observed_states = {}

        self.current_state   = []
        self.agents          = []
        self.training        = []
        self.action_spaces   = []
        self.state_space     = {}
        print("gRPC-Servicer initialized")

    def translate(self, request):
        for station in request.station:
            for agent in request.agent:
                if agent.station == station.id:
                    self.current_state[station.id, agent.id] = agent.oddTime
                else:
                    self.current_state[station.id, agent.id] = 0

    def finishTraining(self, request, context):
        self.training[request.id] = False
        return protobuf.empty_pb2.Empty()

    def makeSwarm(self, request, context):
        print("MAKE_SWARM")
        self.agents        = request.agentCount * [None]
        self.action_spaces = request.agentCount * [None]
        self.training      = request.agentCount * [True]
        return protobuf.empty_pb2.Empty()

    def makeAgent(self, request, context):
        print(f"MAKE_AGENT:ID-{request.messageId}")
        if len(self.traces) == 0:
            self.traces[request.messageId] = []

        self.agents[request.messageId] = Agent.create(agent='dqn',
                                                      states=self.state_space,
                                                      actions=self.action_spaces[request.messageId],
                                                      batch_size=request.batchSize,
                                                      memory=100,
                                                      exploration=0.1,
                                                      tracking='all'
                                                      )
        print("ACTION_SPACE:", self.agents[request.messageId].actions_spec.value())
        return protobuf.empty_pb2.Empty()

    def makeStateSpace(self, request, context):
        print("MAKE_STATE_SPACE")
        self.state_space = dict(type='float', shape=(request.stationCount, request.agentCount),
                                min_value=0, max_value=1)
        self.current_state = np.zeros(shape=self.state_space['shape'])
        print("STATE_SPACE:", self.state_space['shape'])
        return protobuf.empty_pb2.Empty()

    def makeActionSpace(self, request, context):
        print(f"MAKE_ACTION_SPACE:ID-{request.messageId}")
        self.action_spaces[request.messageId] = dict(type='int', num_values=request.stationCount)
        return protobuf.empty_pb2.Empty()

    def act(self, request, context):
        do_inference_only = (not self.training[request.messageId])

        self.translate(request)

        try:
            action = self.agents[request.messageId].act(states=self.current_state,
                                                        independent=do_inference_only,
                                                        deterministic=do_inference_only)

            self.traces[request.messageId].append([np.copy(self.current_state), action, -1])
            self.observed_states[str(self.current_state)] = self.current_state
        except Exception as err:
            print("Failed to choose action")
            print("AGENT:", self.agents[request.messageId].__str__(), self.agents[request.messageId].__dict__)
            print("STATE:", self.current_state)
            print(err)
            raise

        return swarmIntelligence_pb2.Action(id=action)

    def reward(self, request, context):
        do_inference_only = (not self.training[request.messageId])

        try:
            if not do_inference_only:
                self.agents[request.messageId].observe(terminal=request.terminal, reward=request.value)
            self.traces[request.messageId][-1][2] = request.value
        except Exception as err:
            print("Failed to reward agent")
            print("AGENT:", self.agents[request.messageId].__str__(), self.agents[request.messageId].__dict__)
            print("STATE:", self.current_state)
            print("REWARD:", request.value)
            print(err)
            raise

        return protobuf.empty_pb2.Empty()
