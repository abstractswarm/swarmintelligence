import sys
import grpc
from endpoint import handler
from endpoint import swarmIntelligence_pb2_grpc
from concurrent import futures
from signal import signal, SIGINT, SIGTERM


def main():
    serve()


def serve():
    print(f"Starting server on port {sys.argv[1]}")
    server = grpc.server(futures.ThreadPoolExecutor())
    servicer = handler.SwarmIntelligenceServicer()
    swarmIntelligence_pb2_grpc.add_SwarmIntelligenceServicer_to_server(servicer, server)
    server.add_insecure_port(f"[::]:{sys.argv[1]}")
    server.start()

    def handle_signal(*_):
        print("Received shutdown signal")
        servicer.write_traces()
        all_rpcs_done_event = server.stop(5)
        all_rpcs_done_event.wait(5)

    signal(SIGTERM, handle_signal)
    signal(SIGINT, handle_signal)

    server.wait_for_termination()
    print("Server stopped")


if __name__ == '__main__':
    main()
