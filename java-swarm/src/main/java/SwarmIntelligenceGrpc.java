import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.37.0)",
    comments = "Source: swarmIntelligence.proto")
public final class SwarmIntelligenceGrpc {

  private SwarmIntelligenceGrpc() {}

  public static final String SERVICE_NAME = "SwarmIntelligence";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.SwarmSetup,
      com.google.protobuf.Empty> getMakeSwarmMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "makeSwarm",
      requestType = SwarmIntelligenceOuterClass.SwarmSetup.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.SwarmSetup,
      com.google.protobuf.Empty> getMakeSwarmMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.SwarmSetup, com.google.protobuf.Empty> getMakeSwarmMethod;
    if ((getMakeSwarmMethod = SwarmIntelligenceGrpc.getMakeSwarmMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getMakeSwarmMethod = SwarmIntelligenceGrpc.getMakeSwarmMethod) == null) {
          SwarmIntelligenceGrpc.getMakeSwarmMethod = getMakeSwarmMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.SwarmSetup, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "makeSwarm"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.SwarmSetup.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("makeSwarm"))
              .build();
        }
      }
    }
    return getMakeSwarmMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.AgentSetup,
      com.google.protobuf.Empty> getMakeAgentMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "makeAgent",
      requestType = SwarmIntelligenceOuterClass.AgentSetup.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.AgentSetup,
      com.google.protobuf.Empty> getMakeAgentMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.AgentSetup, com.google.protobuf.Empty> getMakeAgentMethod;
    if ((getMakeAgentMethod = SwarmIntelligenceGrpc.getMakeAgentMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getMakeAgentMethod = SwarmIntelligenceGrpc.getMakeAgentMethod) == null) {
          SwarmIntelligenceGrpc.getMakeAgentMethod = getMakeAgentMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.AgentSetup, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "makeAgent"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.AgentSetup.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("makeAgent"))
              .build();
        }
      }
    }
    return getMakeAgentMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.StateSpace,
      com.google.protobuf.Empty> getMakeStateSpaceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "makeStateSpace",
      requestType = SwarmIntelligenceOuterClass.StateSpace.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.StateSpace,
      com.google.protobuf.Empty> getMakeStateSpaceMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.StateSpace, com.google.protobuf.Empty> getMakeStateSpaceMethod;
    if ((getMakeStateSpaceMethod = SwarmIntelligenceGrpc.getMakeStateSpaceMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getMakeStateSpaceMethod = SwarmIntelligenceGrpc.getMakeStateSpaceMethod) == null) {
          SwarmIntelligenceGrpc.getMakeStateSpaceMethod = getMakeStateSpaceMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.StateSpace, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "makeStateSpace"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.StateSpace.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("makeStateSpace"))
              .build();
        }
      }
    }
    return getMakeStateSpaceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.ActionSpace,
      com.google.protobuf.Empty> getMakeActionSpaceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "makeActionSpace",
      requestType = SwarmIntelligenceOuterClass.ActionSpace.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.ActionSpace,
      com.google.protobuf.Empty> getMakeActionSpaceMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.ActionSpace, com.google.protobuf.Empty> getMakeActionSpaceMethod;
    if ((getMakeActionSpaceMethod = SwarmIntelligenceGrpc.getMakeActionSpaceMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getMakeActionSpaceMethod = SwarmIntelligenceGrpc.getMakeActionSpaceMethod) == null) {
          SwarmIntelligenceGrpc.getMakeActionSpaceMethod = getMakeActionSpaceMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.ActionSpace, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "makeActionSpace"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.ActionSpace.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("makeActionSpace"))
              .build();
        }
      }
    }
    return getMakeActionSpaceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.State,
      SwarmIntelligenceOuterClass.Action> getActMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "act",
      requestType = SwarmIntelligenceOuterClass.State.class,
      responseType = SwarmIntelligenceOuterClass.Action.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.State,
      SwarmIntelligenceOuterClass.Action> getActMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.State, SwarmIntelligenceOuterClass.Action> getActMethod;
    if ((getActMethod = SwarmIntelligenceGrpc.getActMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getActMethod = SwarmIntelligenceGrpc.getActMethod) == null) {
          SwarmIntelligenceGrpc.getActMethod = getActMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.State, SwarmIntelligenceOuterClass.Action>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "act"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.State.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.Action.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("act"))
              .build();
        }
      }
    }
    return getActMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Reward,
      com.google.protobuf.Empty> getRewardMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "reward",
      requestType = SwarmIntelligenceOuterClass.Reward.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Reward,
      com.google.protobuf.Empty> getRewardMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Reward, com.google.protobuf.Empty> getRewardMethod;
    if ((getRewardMethod = SwarmIntelligenceGrpc.getRewardMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getRewardMethod = SwarmIntelligenceGrpc.getRewardMethod) == null) {
          SwarmIntelligenceGrpc.getRewardMethod = getRewardMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.Reward, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "reward"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.Reward.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("reward"))
              .build();
        }
      }
    }
    return getRewardMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Agent,
      com.google.protobuf.Empty> getFinishTrainingMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "finishTraining",
      requestType = SwarmIntelligenceOuterClass.Agent.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Agent,
      com.google.protobuf.Empty> getFinishTrainingMethod() {
    io.grpc.MethodDescriptor<SwarmIntelligenceOuterClass.Agent, com.google.protobuf.Empty> getFinishTrainingMethod;
    if ((getFinishTrainingMethod = SwarmIntelligenceGrpc.getFinishTrainingMethod) == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        if ((getFinishTrainingMethod = SwarmIntelligenceGrpc.getFinishTrainingMethod) == null) {
          SwarmIntelligenceGrpc.getFinishTrainingMethod = getFinishTrainingMethod =
              io.grpc.MethodDescriptor.<SwarmIntelligenceOuterClass.Agent, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "finishTraining"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SwarmIntelligenceOuterClass.Agent.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new SwarmIntelligenceMethodDescriptorSupplier("finishTraining"))
              .build();
        }
      }
    }
    return getFinishTrainingMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SwarmIntelligenceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceStub>() {
        @java.lang.Override
        public SwarmIntelligenceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SwarmIntelligenceStub(channel, callOptions);
        }
      };
    return SwarmIntelligenceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SwarmIntelligenceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceBlockingStub>() {
        @java.lang.Override
        public SwarmIntelligenceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SwarmIntelligenceBlockingStub(channel, callOptions);
        }
      };
    return SwarmIntelligenceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SwarmIntelligenceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SwarmIntelligenceFutureStub>() {
        @java.lang.Override
        public SwarmIntelligenceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SwarmIntelligenceFutureStub(channel, callOptions);
        }
      };
    return SwarmIntelligenceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class SwarmIntelligenceImplBase implements io.grpc.BindableService {

    /**
     */
    public void makeSwarm(SwarmIntelligenceOuterClass.SwarmSetup request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMakeSwarmMethod(), responseObserver);
    }

    /**
     */
    public void makeAgent(SwarmIntelligenceOuterClass.AgentSetup request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMakeAgentMethod(), responseObserver);
    }

    /**
     */
    public void makeStateSpace(SwarmIntelligenceOuterClass.StateSpace request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMakeStateSpaceMethod(), responseObserver);
    }

    /**
     */
    public void makeActionSpace(SwarmIntelligenceOuterClass.ActionSpace request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMakeActionSpaceMethod(), responseObserver);
    }

    /**
     */
    public void act(SwarmIntelligenceOuterClass.State request,
        io.grpc.stub.StreamObserver<SwarmIntelligenceOuterClass.Action> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getActMethod(), responseObserver);
    }

    /**
     */
    public void reward(SwarmIntelligenceOuterClass.Reward request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRewardMethod(), responseObserver);
    }

    /**
     */
    public void finishTraining(SwarmIntelligenceOuterClass.Agent request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFinishTrainingMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getMakeSwarmMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.SwarmSetup,
                com.google.protobuf.Empty>(
                  this, METHODID_MAKE_SWARM)))
          .addMethod(
            getMakeAgentMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.AgentSetup,
                com.google.protobuf.Empty>(
                  this, METHODID_MAKE_AGENT)))
          .addMethod(
            getMakeStateSpaceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.StateSpace,
                com.google.protobuf.Empty>(
                  this, METHODID_MAKE_STATE_SPACE)))
          .addMethod(
            getMakeActionSpaceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.ActionSpace,
                com.google.protobuf.Empty>(
                  this, METHODID_MAKE_ACTION_SPACE)))
          .addMethod(
            getActMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.State,
                SwarmIntelligenceOuterClass.Action>(
                  this, METHODID_ACT)))
          .addMethod(
            getRewardMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.Reward,
                com.google.protobuf.Empty>(
                  this, METHODID_REWARD)))
          .addMethod(
            getFinishTrainingMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                SwarmIntelligenceOuterClass.Agent,
                com.google.protobuf.Empty>(
                  this, METHODID_FINISH_TRAINING)))
          .build();
    }
  }

  /**
   */
  public static final class SwarmIntelligenceStub extends io.grpc.stub.AbstractAsyncStub<SwarmIntelligenceStub> {
    private SwarmIntelligenceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SwarmIntelligenceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SwarmIntelligenceStub(channel, callOptions);
    }

    /**
     */
    public void makeSwarm(SwarmIntelligenceOuterClass.SwarmSetup request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMakeSwarmMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void makeAgent(SwarmIntelligenceOuterClass.AgentSetup request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMakeAgentMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void makeStateSpace(SwarmIntelligenceOuterClass.StateSpace request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMakeStateSpaceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void makeActionSpace(SwarmIntelligenceOuterClass.ActionSpace request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMakeActionSpaceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void act(SwarmIntelligenceOuterClass.State request,
        io.grpc.stub.StreamObserver<SwarmIntelligenceOuterClass.Action> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getActMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void reward(SwarmIntelligenceOuterClass.Reward request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRewardMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void finishTraining(SwarmIntelligenceOuterClass.Agent request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFinishTrainingMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SwarmIntelligenceBlockingStub extends io.grpc.stub.AbstractBlockingStub<SwarmIntelligenceBlockingStub> {
    private SwarmIntelligenceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SwarmIntelligenceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SwarmIntelligenceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty makeSwarm(SwarmIntelligenceOuterClass.SwarmSetup request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMakeSwarmMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty makeAgent(SwarmIntelligenceOuterClass.AgentSetup request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMakeAgentMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty makeStateSpace(SwarmIntelligenceOuterClass.StateSpace request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMakeStateSpaceMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty makeActionSpace(SwarmIntelligenceOuterClass.ActionSpace request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMakeActionSpaceMethod(), getCallOptions(), request);
    }

    /**
     */
    public SwarmIntelligenceOuterClass.Action act(SwarmIntelligenceOuterClass.State request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getActMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty reward(SwarmIntelligenceOuterClass.Reward request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRewardMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty finishTraining(SwarmIntelligenceOuterClass.Agent request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFinishTrainingMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SwarmIntelligenceFutureStub extends io.grpc.stub.AbstractFutureStub<SwarmIntelligenceFutureStub> {
    private SwarmIntelligenceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SwarmIntelligenceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SwarmIntelligenceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> makeSwarm(
        SwarmIntelligenceOuterClass.SwarmSetup request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMakeSwarmMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> makeAgent(
        SwarmIntelligenceOuterClass.AgentSetup request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMakeAgentMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> makeStateSpace(
        SwarmIntelligenceOuterClass.StateSpace request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMakeStateSpaceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> makeActionSpace(
        SwarmIntelligenceOuterClass.ActionSpace request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMakeActionSpaceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<SwarmIntelligenceOuterClass.Action> act(
        SwarmIntelligenceOuterClass.State request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getActMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> reward(
        SwarmIntelligenceOuterClass.Reward request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRewardMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> finishTraining(
        SwarmIntelligenceOuterClass.Agent request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFinishTrainingMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_MAKE_SWARM = 0;
  private static final int METHODID_MAKE_AGENT = 1;
  private static final int METHODID_MAKE_STATE_SPACE = 2;
  private static final int METHODID_MAKE_ACTION_SPACE = 3;
  private static final int METHODID_ACT = 4;
  private static final int METHODID_REWARD = 5;
  private static final int METHODID_FINISH_TRAINING = 6;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SwarmIntelligenceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SwarmIntelligenceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_MAKE_SWARM:
          serviceImpl.makeSwarm((SwarmIntelligenceOuterClass.SwarmSetup) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_MAKE_AGENT:
          serviceImpl.makeAgent((SwarmIntelligenceOuterClass.AgentSetup) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_MAKE_STATE_SPACE:
          serviceImpl.makeStateSpace((SwarmIntelligenceOuterClass.StateSpace) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_MAKE_ACTION_SPACE:
          serviceImpl.makeActionSpace((SwarmIntelligenceOuterClass.ActionSpace) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_ACT:
          serviceImpl.act((SwarmIntelligenceOuterClass.State) request,
              (io.grpc.stub.StreamObserver<SwarmIntelligenceOuterClass.Action>) responseObserver);
          break;
        case METHODID_REWARD:
          serviceImpl.reward((SwarmIntelligenceOuterClass.Reward) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_FINISH_TRAINING:
          serviceImpl.finishTraining((SwarmIntelligenceOuterClass.Agent) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SwarmIntelligenceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SwarmIntelligenceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return SwarmIntelligenceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SwarmIntelligence");
    }
  }

  private static final class SwarmIntelligenceFileDescriptorSupplier
      extends SwarmIntelligenceBaseDescriptorSupplier {
    SwarmIntelligenceFileDescriptorSupplier() {}
  }

  private static final class SwarmIntelligenceMethodDescriptorSupplier
      extends SwarmIntelligenceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SwarmIntelligenceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SwarmIntelligenceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SwarmIntelligenceFileDescriptorSupplier())
              .addMethod(getMakeSwarmMethod())
              .addMethod(getMakeAgentMethod())
              .addMethod(getMakeStateSpaceMethod())
              .addMethod(getMakeActionSpaceMethod())
              .addMethod(getActMethod())
              .addMethod(getRewardMethod())
              .addMethod(getFinishTrainingMethod())
              .build();
        }
      }
    }
    return result;
  }
}
