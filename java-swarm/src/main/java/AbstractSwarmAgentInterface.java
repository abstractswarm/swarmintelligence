/*
AbstractSwarm agent that does not make any decisions
Copyright (C) 2020  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides the three methods of the AbstractSwarm agent interface
 * for state perception and performing actions, communication with other agents
 * and the perception of rewards.<br />
 * <br />
 *
 * The properties of the interface's objects are:<br />
 * <br />
 *
 * AGENT:<br />
 * .name           the agent's name<br />
 * .type           the agent's AGENT_TYPE (see below)<br />
 * .frequency      the number of remaining visits; -1 if the agent's type has
 *                 no frequency attribute<br />
 * .necessities    the number of remaining visits for each connected station,
 *                 -1 if the agent's type has no necessity attribute<br />
 * .cycles         the number of remaining cycles for each incoming visit edge;
 *                 -1 if agent's type has no cycle attribute<br />
 * .time           the remaining time on the target station, -1 if agent is
 *                 currently not visiting a station or if agent's type has no
 *                 time attribute<br />
 * .target         the agent's current target<br />
 * .visiting       whether the agent is currently visiting a station<br />
 * <br />
 *
 * STATION:<br />
 * .name           the station's name<br />
 * .type           the station's STATION_TYPE (see below)<br />
 * .frequency      the number of remaining visits; -1 if the station's type
 *                 has no frequency attribute<br />
 * .necessities    the number of remaining visits for each connected agent,
 *                 -1 if the station's type has no necessity attribute<br />
 * .cycles         the number of remaining cycles for each incoming visit edge;
 *                 -1 if station's type has no cycle attribute<br />
 * .space          the remaining space, -1 if the station's type has no space
 *                 attribute<br />
 * <br />
 *
 * AGENT_TYPE:<br />
 * .name           the agent type's name as string<br />
 * .type           the type as string ("AGENT_TYPE")<br />
 * .components     the agent type's AGENTs (see above)<br />
 * .frequency      the agent type's frequency attribute; -1 if the agent type
 *                 has no frequency attribute<br />
 * .necessity      the agent type's necessity attribute; -1 if the agent type
 *                 has no necessity attribute<br />
 * .cycle          the agent type's cycle attribute; -1 if agent type has no
 *                 cycle attribute<br />
 * .time           the agent type's time attribute, -1 if agent type has no
 *                 time attribute<br />
 * .size           the agent type's size attribute, -1 if agent type has no
 *                 size attribute<br />
 * .priority       the agent type's priority attribute, -1 if agent type has
 *                 no priority attribute<br />
 * .visitEdges     the agent type's VISIT_EDGEs as list (see below)<br />
 * .timeEdges      the agent type's TIME_EDGEs as list (see below)<br />
 * .placeEdges     the agent type's PLACE_EDGEs as list (see below)
 *                 (note that, by definition, agent types cannot have place
 *                 edges and thus this list will always be empty; this is only
 *                 for backwards compatibility/unification reasons)<br />
 * <br />
 *
 * STATION_TYPE:<br />
 * .name           the station type's name as string<br />
 * .type           the type as string ("STATION_TYPE")<br />
 * .components     the station type's stations<br />
 * .frequency      the station type's frequency attribute; -1 if the station
 *                 type has no frequency attribute<br />
 * .necessity      the station type's necessity attribute; -1 if the station
 *                 type has no necessity attribute<br />
 * .cycle          the station type's cycle attribute; -1 if station type has
 *                 no cycle attribute<br />
 * .time           the station type's time attribute, -1 if station type has no
 *                 time attribute<br />
 * .space          the station type's space attribute, -1 if station type has
 *                 no space attribute<br />
 * .visitEdges     the station type's VISIT_EDGEs as list (see below)<br />
 * .timeEdges      the station type's TIME_EDGEs as list (see below)<br />
 * .placeEdges     the station type's PLACE_EDGEs as list (see below)<br />
 * <br />
 *
 * VISIT EDGE:<br />
 * .type           the edge's type as string ("VISIT_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .bold           whether the edge is bold<br />
 * <br />
 *
 * TIME EDGE:<br />
 * .type           the edge's type as string ("TIME_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .incoming       whether the edge is incoming<br />
 * .outgoing       whether the edge is outgoing<br />
 * .andConnected   whether the edge is and-connected to the opposite type<br />
 * .andOrigin      whether the edge is and-connected at its origin type<br />
 * <br />
 *
 * PLACE EDGE:<br />
 * .type         the edge's type as string ("PLACE_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .incoming       whether the edge is incoming<br />
 * .outgoing       whether the edge is outgoing<br />
 */
public class AbstractSwarmAgentInterface
{
    // Number of training episodes
    private static final long TRAINING_EPISODES = Long.parseLong(System.getenv("TRAINING").trim());
    // Current amount of episodes
    private static long episodeCount = 0;
    // Keep track which agent has already finished training
    private static Map<Agent, Boolean> trainingFinished = new HashMap<>();


    // Declaring stub for interaction with the rpc server
    //
    private final static SwarmIntelligenceGrpc.SwarmIntelligenceBlockingStub BLOCKING_STUB;


    // Maps for tracking numeric ids of the simulation objects.
    // The backend handles numeric values only.
    //
    private static Map<Station, Integer>  stationIdMap    = new HashMap<>();
    private static Map<Agent, Integer>    agentIdMap      = new HashMap<>();

    // Storing time of decision per agent
    //
    private static Map<Agent, Long> decisionTimeMap = new HashMap<>();

    // Storing positions of agents
    //
    private static Map<Agent, Station> positionMap = new HashMap<>();


    // Keeping track of the mapping from action-id to station, per agent
    //
    private static Map<Agent, BiMap<Station, Integer>> actionSpaceMap = new HashMap<>();

    // Initialization flag as the simulation interface does not provide a init call
    //
    private static boolean init = false;


    // Keeping track of the calls to the evaluation function
    private static Agent previousMe = null;
    private static long previousTime = -1;
    private static Map<Agent, Station> chosenStation = new HashMap<>();


    /**
     * Static-Block for setting up grpc
     */
    static {
        final String TARGET_ADDRESS = new String("localhost:").concat(System.getenv("TFPORT")).trim();
        ManagedChannel channel = ManagedChannelBuilder.forTarget(TARGET_ADDRESS).usePlaintext().build();
        BLOCKING_STUB = SwarmIntelligenceGrpc.newBlockingStub(channel);
    }

    /**
     * Initialization.
     */
    private static void initialize(Agent me, HashMap<Agent, Object> agents, List<Station> stations) {
        int idx;

        idx = 0;
        for (Station stn : stations) {
            stationIdMap.put(stn, idx);
            idx++;
        }
        idx = 0;
        agentIdMap.put(me, idx);
        decisionTimeMap.put(me, 0L);
        idx++;
        for (Agent agt : agents.keySet()) {
            agentIdMap.put(agt, idx);
            decisionTimeMap.put(agt, 0L);
            idx++;
        }

        SwarmIntelligenceOuterClass.SwarmSetup setup = SwarmIntelligenceOuterClass.SwarmSetup.newBuilder()
                .setAgentCount(agentIdMap.size())
        .build();
        System.out.println();
        System.out.println("----------------");
        System.out.println("RPC: makeSwarm");
        BLOCKING_STUB.makeSwarm(setup);
        System.out.println("----------------");
        System.out.println();

        SwarmIntelligenceOuterClass.StateSpace stateSpace = SwarmIntelligenceOuterClass.StateSpace.newBuilder()
                .setAgentCount(agentIdMap.size())
                .setStationCount(stationIdMap.size())
        .build();

        System.out.println();
        System.out.println("----------------");
        System.out.println("RPC: makeStateSpace");
        BLOCKING_STUB.makeStateSpace(stateSpace);
        System.out.printf("%dx%d\n", stateSpace.getAgentCount(), stateSpace.getStationCount());
        System.out.println("----------------");
        System.out.println();

        for(Agent agt : agentIdMap.keySet()) {

            BiMap<Station, Integer> actionMapping = HashBiMap.create();
            idx = 0;

            for(VisitEdge edge : agt.type.visitEdges) {
                List<Station> connectedStations = ((StationType)(edge.connectedType)).components;
                for(Station stn : connectedStations) {
                    actionMapping.put(stn, idx++);
                }
            }

            SwarmIntelligenceOuterClass.ActionSpace actionSpace = SwarmIntelligenceOuterClass.ActionSpace.newBuilder()
                    .setMessageId(agentIdMap.get(agt))
                    .setStationCount(actionMapping.size())
            .build();

            System.out.println();
            System.out.println("----------------");
            System.out.printf("RPC: makeActionSpace for %s\n", agt.name);
            BLOCKING_STUB.makeActionSpace(actionSpace);
            System.out.println("----------------");
            System.out.println();

            actionSpaceMap.put(agt, actionMapping);

            SwarmIntelligenceOuterClass.AgentSetup agentSetup = SwarmIntelligenceOuterClass.AgentSetup.newBuilder()
                    .setMessageId(agentIdMap.get(agt))
                    .setBatchSize(1)
            .build();

            System.out.println();
            System.out.println("----------------");
            System.out.printf("RPC: makeAgent %s\n", agt.name);
            BLOCKING_STUB.makeAgent(agentSetup);
            System.out.println("----------------");
            System.out.println();
        }

        System.out.println("ACTIONSPACE-MAP");
        for(Agent agt : actionSpaceMap.keySet()) {
            System.out.printf("Agent-%s\n", agt.name);
            for (Station stn : actionSpaceMap.get(agt).keySet())
                System.out.printf("\t%s--%d\n", stn.name, actionSpaceMap.get(agt).get(stn));
        }
        System.out.println();

        for (Agent agt : agentIdMap.keySet()) {
            trainingFinished.put(agt, false);
        }

        init = true;
    }

    /**
     * This method allows an agent to perceive its current state and to perform
     * actions by returning an evaluation value for potential next target
     * stations. The method is called for every station that can be visted by
     * the agent.
     *
     * @param me        the agent itself
     * @param others    all other agents in the scenario with their currently
     *                  communicated information
     * @param stations  all stations in the scenario
     * @param time      the current time unit
     * @param station   the station to be evaluated as potential next target
     *
     * @return          the evaluation value of the station
     */
    public static double evaluation( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, Station station )
    {
        // Initialize on the very first call of this function,
        // the simulation's logic guarantees the evaluation function to be called in front of all other calls.
        if ( !init ) initialize(me, others, stations);

        boolean newEpisode = time < previousTime;
        if (newEpisode) {
            episodeCount++;
            System.out.printf("Episode %d finished\n", episodeCount);
            for(Agent agt : chosenStation.keySet()) {
                if(chosenStation.get(agt) != null) reward(agt, others, stations, time, 0.5);
            }
        }

        if (!trainingFinished.get(me) && episodeCount > TRAINING_EPISODES) {
            trainingFinished.put(me, true);
            SwarmIntelligenceOuterClass.Agent agt = SwarmIntelligenceOuterClass.Agent.newBuilder()
                    .setId(agentIdMap.get(me))
            .build();
            System.out.println();
            System.out.println("----------------");
            System.out.println("RPC: finishTraining");
            System.out.println(me.name);
            BLOCKING_STUB.finishTraining(agt);
            System.out.println("----------------");
            System.out.println();
        }

        System.out.printf("\n..............\nTIME\t%d\n..............\n", time);

        boolean hasChosen = (time == previousTime && me == previousMe);
        System.out.printf("hasChosen=%s\n", hasChosen);

        if (hasChosen == false) {
            Station stn = choose(me, others, stations, time);
            chosenStation.put(me, stn);
        }

        double score = (chosenStation.get(me) == station) ? 1 : 0 ;

        if(chosenStation.get(me) != null) {
            System.out.printf("Chosen Station: %s\n", chosenStation.get(me).name);
            System.out.printf("Presented Station: %s\n", station.name);
        }

        System.out.printf("Score: %g\n", score);

        previousTime = time;
        previousMe   = me;

        return score;
    }


    /**
     * Choose the action to take given the current state
     * @param me        the agent itself
     * @param others    all other agents in the scenario with their currently
     *                  communicated information
     * @param stations  all stations in the scenario
     * @param time      the current time unit
     *
     */
    private static Station choose(Agent me, HashMap<Agent, Object> others, List<Station> stations, long time) {
        // Build State message

        // Collect all stations and add them into the state message with their global IDs
        SwarmIntelligenceOuterClass.State.Builder stateBuilder = SwarmIntelligenceOuterClass.State.newBuilder();
        for(Station stationObject : stations) {
            SwarmIntelligenceOuterClass.Station stn = SwarmIntelligenceOuterClass.Station.newBuilder()
                .setId(stationIdMap.get(stationObject))
            .build();

            stateBuilder.addStation(stn);
        }

        // Collect all agents and add them into the state message with their global IDs
        // as well as their target station and the remaining time to reach it, normalized to one.
        for(Agent agt : others.keySet()) {

            int agentId = agentIdMap.get(agt);

            int agentTarget     = -1;
            double agentOddTime = 0;

            if (others.get(agt) != null) {
                long agentEta = (long) ((Object[]) others.get(agt))[1];
                long maxDuration = agentEta - decisionTimeMap.get(agt);
                agentOddTime = (maxDuration != 0) ? (Double.valueOf(agentEta - time) / Double.valueOf(maxDuration)) : 0;
                agentTarget   =  stationIdMap.get((Station)(((Object[])others.get(agt))[0]));
            }

            SwarmIntelligenceOuterClass.Agent agent = SwarmIntelligenceOuterClass.Agent.newBuilder()
                .setId(agentId)
                .setStation(agentTarget)
                .setOddTime(agentOddTime)
            .build();

            stateBuilder.addAgent(agent);
        }
        
        int agentTarget = (positionMap.get(me) != null) ? stationIdMap.get(positionMap.get(me)) : -1;
        SwarmIntelligenceOuterClass.Agent agent = SwarmIntelligenceOuterClass.Agent.newBuilder()
            .setId(agentIdMap.get(me))
            .setStation(agentTarget)
            .setOddTime(0.0)
        .build();

        stateBuilder.addAgent(agent);

        SwarmIntelligenceOuterClass.State state = stateBuilder.setMessageId(agentIdMap.get(me)).build();

        Station chosenStation = null;
        System.out.println();
        System.out.println("----------------");
        System.out.println("RPC: act");
        System.out.printf("Choose station %s\n", me.name);
        SwarmIntelligenceOuterClass.Action action = BLOCKING_STUB.act(state);
        chosenStation = actionSpaceMap.get(me).inverse().get(action.getId());
        decisionTimeMap.put(me, time);
        System.out.println(chosenStation.name);
        System.out.println("----------------");
        System.out.println();

        // Return station that corresponds to the chosen action ID or null if there was already an evaluation
        return chosenStation;
    }


    /**
     * This method allows an agent to communicate with other agents by
     * returning a communication data object.
     *
     * @param me           the agent itself
     * @param others       all other agents in the scenario with their
     *                     currently communicated information
     * @param stations     all stations in the scenario
     * @param time         the current time unit
     * @param defaultData  a triple (selected station, time unit when the
     *                     station is reached, evaluation value of the station)
     *                     that can be used for default communication
     *
     * @return             the communication data object
     */
    public static Object communication( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, Object[] defaultData )
    {
        return defaultData;
    }


    /**
     * This method allows an agent to perceive the local reward for its most
     * recent action.
     *
     * @param me           the agent itself
     * @param others       all other agents in the scenario with their
     *                     currently communicated information
     * @param stations     all stations in the scenario
     * @param time         the current time unit
     * @param value        the local reward in [0, 1] for the agent's most
     *                     recent action
     */
    public static void reward( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, double value )
    {
        if (trainingFinished.get(me)) return;

        SwarmIntelligenceOuterClass.Reward reward = SwarmIntelligenceOuterClass.Reward.newBuilder()
            .setMessageId(agentIdMap.get(me))
            .setValue(value)
        .build();

        System.out.printf("\n..............\nTIME\t%d\n..............\n", time);
        System.out.println();
        System.out.println("----------------");
        System.out.println("RPC: reward");
        System.out.println(me.name);
        BLOCKING_STUB.reward(reward);
        chosenStation.put(me, null);
        positionMap.put(me, me.target);
        System.out.println(reward);
        System.out.println("----------------");
        System.out.println();

    }
}
