from transitions.extensions import GraphMachine


class MediumFSM(object):
    rewards = {'0,0': 0, '1,1': 1, '0,1,T': 0, '1,1,T': 1}
    transitions = [['1', '0,0', '1,1'],
                   ['0', '0,0', '0,1,T'],
                   ['0', '1,1', '0,1,T'],
                   ['1', '1,1', '1,1,T']]

    def __init__(self):
        self.machine = GraphMachine(use_pygraphviz=False, model=self, states=list(MediumFSM.rewards.keys()),
                                    transitions=MediumFSM.transitions, initial='0,0')
        self.get_graph().draw('MediumFSM.png', prog='dot')

    def reward(self):
        return MediumFSM.rewards[self.state]
    
    def reset(self):
        self.machine.set_state('0,0')
