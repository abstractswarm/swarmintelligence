from tensorforce import Agent
import numpy as np
from tqdm import trange
import matplotlib.pyplot as plt
import swarmIntelligence_pb2
from BasicFSM import BasicFSM
from MediumFSM import MediumFSM


state_space = {'type': 'float', 'shape': (2, 1), 'min_value': 0, 'max_value': 1}
action_space = {'type': 'int', 'num_values': 2}
batch_size = 25
training_episodes = 500
test_episodes = 100
observed_states = {}
state_machine = MediumFSM()


def state_to_pb(fsm_state):
    (dst, odd) = fsm_state.split(',')
    agents = [swarmIntelligence_pb2.Agent(id=0, station=int(dst), oddTime=float(odd))]
    stations = [swarmIntelligence_pb2.Station(id=0), swarmIntelligence_pb2.Station(id=1)]
    return swarmIntelligence_pb2.State(messageId=0, agent=agents, station=stations)


def pb_to_state(pb_state):
    state = np.zeros(shape=state_space['shape'], dtype=state_space['type'])
    for station in pb_state.station:
        for agent in pb_state.agent:
            if agent.station == station.id:
                state[station.id, agent.id] = agent.oddTime
            else:
                state[station.id, agent.id] = 0
    return state


def trace(agent):
    for state_hash, state in observed_states.items():
        agent.act(states=state, independent=True, deterministic=True)
        action_values = agent.tracked_tensors()['agent/policy/action-values']
        print(f'STATE: {state_hash}')
        print(f'ACTIONS: {dict(enumerate(action_values))}')
        print('\n')
        plt.xticks(range(len(action_values)), range(len(action_values)))
        plt.bar(*zip(*enumerate(action_values)))
        plt.title(f'Q Values for state\n{state_hash}')
        plt.show()


def execute(action):
    transition = getattr(state_machine, str(action))
    transition()
    reward = state_machine.reward()
    terminal = state_machine.state.endswith('T')
    return terminal, reward


def run_episode(agent, state, testing):
    action = agent.act(states=state, independent=testing, deterministic=testing)
    observed_states[str(state)] = state
    terminal, reward = execute(action)
    if not testing:
        agent.observe(terminal=False, reward=reward)
    return terminal


def run(agent, episodes, testing):
    for _ in trange(episodes):
        state_machine.reset()
        terminal = False
        while not terminal:
            fsm_state = state_machine.state
            pb_state = state_to_pb(fsm_state)
            state = pb_to_state(pb_state)
            terminal = run_episode(agent, state, testing)


def main():
    agent = Agent.create(agent='dqn', states=state_space, actions=action_space,
                         batch_size=batch_size, memory=100, exploration=0.1, tracking='all')
    run(agent, training_episodes, False)
    run(agent, test_episodes, True)
    trace(agent)
    agent.close()


if __name__ == '__main__':
    main()
