from transitions.extensions import GraphMachine


class BasicFSM(object):
    rewards = {'0,0': 0, '1,0,T': 0, '0,1,T': 1}
    transitions = [['0', '0,0', '1,0,T'],
                   ['1', '0,0', '0,1,T']]

    def __init__(self):
        self.machine = GraphMachine(use_pygraphviz=False, model=self, states=list(BasicFSM.rewards.keys()),
                                    transitions=BasicFSM.transitions, initial='0,0')
        self.get_graph().draw('BasicFSM.png', prog='dot')

    def reward(self):
        return BasicFSM.rewards[self.state]

    def reset(self):
        self.machine.set_state('0,0')
