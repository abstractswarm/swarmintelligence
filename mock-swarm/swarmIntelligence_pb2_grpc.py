# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2
import swarmIntelligence_pb2 as swarmIntelligence__pb2


class SwarmIntelligenceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.makeSwarm = channel.unary_unary(
                '/SwarmIntelligence/makeSwarm',
                request_serializer=swarmIntelligence__pb2.SwarmSetup.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.makeAgent = channel.unary_unary(
                '/SwarmIntelligence/makeAgent',
                request_serializer=swarmIntelligence__pb2.AgentSetup.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.makeStateSpace = channel.unary_unary(
                '/SwarmIntelligence/makeStateSpace',
                request_serializer=swarmIntelligence__pb2.StateSpace.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.makeActionSpace = channel.unary_unary(
                '/SwarmIntelligence/makeActionSpace',
                request_serializer=swarmIntelligence__pb2.ActionSpace.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.act = channel.unary_unary(
                '/SwarmIntelligence/act',
                request_serializer=swarmIntelligence__pb2.State.SerializeToString,
                response_deserializer=swarmIntelligence__pb2.Action.FromString,
                )
        self.reward = channel.unary_unary(
                '/SwarmIntelligence/reward',
                request_serializer=swarmIntelligence__pb2.Reward.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.finishTraining = channel.unary_unary(
                '/SwarmIntelligence/finishTraining',
                request_serializer=swarmIntelligence__pb2.Agent.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )


class SwarmIntelligenceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def makeSwarm(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def makeAgent(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def makeStateSpace(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def makeActionSpace(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def act(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def reward(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def finishTraining(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_SwarmIntelligenceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'makeSwarm': grpc.unary_unary_rpc_method_handler(
                    servicer.makeSwarm,
                    request_deserializer=swarmIntelligence__pb2.SwarmSetup.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'makeAgent': grpc.unary_unary_rpc_method_handler(
                    servicer.makeAgent,
                    request_deserializer=swarmIntelligence__pb2.AgentSetup.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'makeStateSpace': grpc.unary_unary_rpc_method_handler(
                    servicer.makeStateSpace,
                    request_deserializer=swarmIntelligence__pb2.StateSpace.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'makeActionSpace': grpc.unary_unary_rpc_method_handler(
                    servicer.makeActionSpace,
                    request_deserializer=swarmIntelligence__pb2.ActionSpace.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'act': grpc.unary_unary_rpc_method_handler(
                    servicer.act,
                    request_deserializer=swarmIntelligence__pb2.State.FromString,
                    response_serializer=swarmIntelligence__pb2.Action.SerializeToString,
            ),
            'reward': grpc.unary_unary_rpc_method_handler(
                    servicer.reward,
                    request_deserializer=swarmIntelligence__pb2.Reward.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'finishTraining': grpc.unary_unary_rpc_method_handler(
                    servicer.finishTraining,
                    request_deserializer=swarmIntelligence__pb2.Agent.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'SwarmIntelligence', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class SwarmIntelligence(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def makeSwarm(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/makeSwarm',
            swarmIntelligence__pb2.SwarmSetup.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def makeAgent(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/makeAgent',
            swarmIntelligence__pb2.AgentSetup.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def makeStateSpace(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/makeStateSpace',
            swarmIntelligence__pb2.StateSpace.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def makeActionSpace(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/makeActionSpace',
            swarmIntelligence__pb2.ActionSpace.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def act(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/act',
            swarmIntelligence__pb2.State.SerializeToString,
            swarmIntelligence__pb2.Action.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def reward(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/reward',
            swarmIntelligence__pb2.Reward.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def finishTraining(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/SwarmIntelligence/finishTraining',
            swarmIntelligence__pb2.Agent.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
