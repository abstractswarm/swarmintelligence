from tqdm import tqdm
from functools import reduce
import operator
import grpc

import swarmIntelligence_pb2
import swarmIntelligence_pb2_grpc
from FSMs.Baseline import Baseline
from FSMs.BasicEnv import BasicEnv
from FSMs.SimpleOne import SimpleOne

scenario = BasicEnv
RUNS = 1
PORT = 50505
TRAINING_EPISODES = 1000
INFERENCE_EPISODES = 250


def make_swarm(stub):
    swarm_setup = swarmIntelligence_pb2.SwarmSetup(agentCount=scenario.agent_count)
    stub.makeSwarm(swarm_setup)


def make_state_space(stub):
    state_space = swarmIntelligence_pb2.StateSpace(agentCount=scenario.agent_count, stationCount=scenario.station_count)
    stub.makeStateSpace(state_space)


def make_action_space(stub, agent_id):
    action_space = swarmIntelligence_pb2.ActionSpace(messageId=agent_id, stationCount=scenario.station_count)
    stub.makeActionSpace(action_space)


def make_agent(stub, agent_id):
    agent_setup = swarmIntelligence_pb2.AgentSetup(messageId=agent_id, batchSize=scenario.batch_size)
    stub.makeAgent(agent_setup)


def finish_training(stub, agent_id):
    agent = swarmIntelligence_pb2.Agent(id=agent_id)
    stub.finishTraining(agent)


def init_grpc():
    channel = grpc.insecure_channel(f'localhost:{PORT}')
    return swarmIntelligence_pb2_grpc.SwarmIntelligenceStub(channel)


def init_swarm(stub):
    make_swarm(stub)
    make_state_space(stub)
    for i in range(scenario.agent_count):
        make_action_space(stub, i)
        make_agent(stub, i)


def act(stub, agent_id, state_machine):
    stations = []
    agents = []

    for i in range(scenario.station_count):
        stations.append(swarmIntelligence_pb2.Station(id=i))
    for i in range(scenario.agent_count):
        fsm_state = state_machine[i].state
        (dst, odd) = fsm_state.split(';')
        agents.append(swarmIntelligence_pb2.Agent(id=i, station=int(dst), oddTime=float(odd)))

    state = swarmIntelligence_pb2.State(messageId=agent_id, agent=agents, station=stations)
    action = stub.act(state)
    return str(action.id)


def reward(stub, agent_id, value, terminal):
    rwd = swarmIntelligence_pb2.Reward(messageId=agent_id, value=value, terminal=terminal)
    stub.reward(rwd)


def run_episode(stub):
    terminal = scenario.agent_count * [False]
    state_machine = scenario.agent_count * [scenario()]
    all_agents_are_terminal = False
    while not all_agents_are_terminal:
        active_agents = [i for i in range(len(terminal)) if not terminal[i]]
        for i in active_agents:
            action = act(stub, i, state_machine)
            transition = getattr(state_machine[i], action)
            transition()
            terminal[i] = state_machine[i].state.endswith('T')
            reward(stub, i, state_machine[i].reward(), terminal[i])
        all_agents_are_terminal = reduce(operator.and_, terminal)


def main():
    print('INIT GRPC')
    stub = init_grpc()
    for i in range(RUNS):
        print(f'RUN {i}')
        run(stub)


def run(stub):
    fsm = scenario()
    fsm.draw()
    print('INIT SWARM')
    init_swarm(stub)
    for i in tqdm(range(TRAINING_EPISODES), desc='TRAINING'):
        run_episode(stub=stub)
    for i in range(scenario.agent_count):
        finish_training(stub, i)
    for i in tqdm(range(INFERENCE_EPISODES), desc='INFERENCE'):
        run_episode(stub=stub)


if __name__ == '__main__':
    main()
