from transitions.extensions import GraphMachine


class Baseline(object):
    agent_count = 1
    station_count = 2
    batch_size = 1
    rewards = {'0;0': 0, '0;1;T': 1, '1;1;T': 0}
    transitions = [['0', '0;0', '0;1;T'],
                   ['1', '0;0', '1;1;T']]

    def __init__(self):
        self.FSM = GraphMachine(use_pygraphviz=False, model=self, states=list(Baseline.rewards.keys()),
                                transitions=Baseline.transitions, initial='0;0')

    def draw(self):
        self.get_graph().draw('BaselineFSM.png', prog='dot')

    def reward(self):
        return Baseline.rewards[self.state]
