from transitions.extensions import GraphMachine


class SimpleOne(object):
    agent_count = 1
    station_count = 2
    batch_size = 1
    rewards = {'0;0': 0, '0;0': 1, '1;1': 0.5, '0;1;T': 0.5, '1;1;T': 0.5}
    transitions = [['0', '0;0', '0;0'],
                   ['0', '0;0', '1;1;T'],
                   ['0', '1;1', '0;1;T'],
                   ['1', '0;0', '1;1'],
                   ['1', '0;0', '1;1;T'],
                   ['1', '1;1', '0;1;T']]

    def __init__(self):
        self.FSM = GraphMachine(use_pygraphviz=False, model=self, states=list(SimpleOne.rewards.keys()),
                                transitions=SimpleOne.transitions, initial='0;0')

    def draw(self):
        self.get_graph().draw('SimpleOneFSM.png', prog='dot')

    def reward(self):
        return SimpleOne.rewards[self.state]
