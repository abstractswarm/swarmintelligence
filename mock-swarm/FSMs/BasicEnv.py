from transitions.extensions import GraphMachine


class BasicEnv(object):
    agent_count = 1
    station_count = 2
    batch_size = 2
    rewards = {'0;0': 0, '1;1': 1, '0;1;T': 0, '1;1;T': 1}
    transitions = [['1', '0;0', '1;1'],
                   ['0', '0;0', '0;1;T'],
                   ['0', '1;1', '0;1;T'],
                   ['1', '1;1', '1;1;T']]

    def __init__(self):
        self.FSM = GraphMachine(use_pygraphviz=False, model=self, states=list(BasicEnv.rewards.keys()),
                                transitions=BasicEnv.transitions, initial='0;0')

    def draw(self):
        self.get_graph().draw('BasicEnvFSM.png', prog='dot')

    def reward(self):
        return BasicEnv.rewards[self.state]
