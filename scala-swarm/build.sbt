lazy val buildSettings = Seq(
  organization := "de.imbei",
  version := "0.1.0",
  scalaVersion := "2.12.10"
)

lazy val dependencies = Seq(
 
)

lazy val swarmintelligence = (project in file("."))
  .settings(
    buildSettings,
    libraryDependencies ++= dependencies
  )