import java.util.List;
import java.util.Map;

class ModelDataTypes {}

class AbstractSwarmObject { }

class ComponentType extends AbstractSwarmObject
{
    public String name;
    public String type;

    public int frequency;
    public int necessity;
    public int time;
    public int cycle;

    public List<VisitEdge> visitEdges;
    public List<TimeEdge> timeEdges;
    public List<PlaceEdge> placeEdges;
}

class StationType extends ComponentType
{
    public List<Station> components;

    public int space;
}

class AgentType extends ComponentType
{
    public List<Agent> components;

    public int size;
    public int priority;
}

class Edge extends AbstractSwarmObject
{
    public ComponentType connectedType;
    public String type;
}

class VisitEdge extends Edge
{
    public boolean bold;
}

class WeightedEdge extends Edge
{
    public boolean incoming;
    public boolean outgoing;
    public int weight;
}

class PlaceEdge extends WeightedEdge { }

class TimeEdge extends WeightedEdge
{
    public boolean andOrigin;
    public boolean andConnected;
}

class Component extends AbstractSwarmObject
{
    public String name;

    public int frequency;
    public Map<TimeEdge, Integer> cycles;
}

class Station extends Component
{
    public StationType type;

    public int space;
    public Map<Agent, Integer> necessities;
}

class Agent extends Component
{
    public AgentType type;

    public int time;
    public Map<Station, Integer> necessities;
    public Station target;
    public Station previousTarget;
    public boolean visiting;
}