import java.util.HashMap
import java.util.List

object SmartAgent {
  def evaluate(me: Agent, others: HashMap[Agent, Any], stations: List[Station], time: Long, station: Station): Double = {
    println("EVALUATE")
    return 0
  }

  def communicate(me: Agent, others: HashMap[Agent, Any], stations: List[Station], time: Long, defaultData: Array[Any]): Any = {
    println("COMMUNICATE")
    return null
  }

  def reward(me: Agent, others: HashMap[Agent, Any], stations: List[Station], time: Long, value: Double): Unit = {
    println("REWARD")
  }
}